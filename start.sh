#!/bin/bash

set -eu

mkdir -p /run/wreeto/log /run/wreeto/tmp

export ENCRYPTION_KEY=0d9f5796221d3fe2d9a42123ad4c5efa
export POSTGRES_HOST=${CLOUDRON_POSTGRESQL_HOST}
export POSTGRES_USER=${CLOUDRON_POSTGRESQL_USERNAME}
export POSTGRES_PASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD}
export POSTGRES_DB=${CLOUDRON_POSTGRESQL_DATABASE}
export POSTGRES_PORT=${CLOUDRON_POSTGRESQL_PORT}
export RACK_ENV=production
export RAILS_ENV=production
export RAILS_MASTER_KEY=f21ab619787e22f6cb13d3884ca20d78
export REDIS_HOST=${CLOUDRON_REDIS_URL}
export REDIS_PASSWORD=${CLOUDRON_REDIS_PASSWORD}
export SMTP_USERNAME=
export SMTP_PASSWORD=
export WREETO_HOST=${CLOUDRON_APP_DOMAIN}
export WREETO_PORT=80
export RAILS_LOG_TO_STDOUT=1
export RAILS_SERVE_STATIC_FILES=true

rm -f /app/code/tmp/pids/server.pid

if [[ ! -f /app/data/.dbsetup ]]; then
    echo "==> initializing db on first run"
    # db:setup creates the database, so we use db:seed and db:migrate instead
    gosu cloudron:cloudron bundle exec rake db:migrate --trace 
    gosu cloudron:cloudron bundle exec rake db:seed --trace || true
    touch /app/data/.dbsetup
fi

echo "==> running db migration"
gosu cloudron:cloudron bundle exec rake db:migrate --trace 

chown -R cloudron:cloudron /app/data /run/wreeto

echo "==> starting wreeto"
exec gosu cloudron:cloudron bundle exec rails server

