FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

ARG VERSION=2.6.8

EXPOSE 8000

# RUN apt-get install \
#       binutils-gold \
#       build-base \
#       curl \
#       file \
#       g++ \
#       gcc \
#       git \
#       less \
#       libstdc++ \
#       libffi-dev \
#       libc-dev \ 
#       linux-headers \
#       libxml2-dev \
#       libxslt-dev \
#       libgcrypt-dev \
#       make \
#       netcat-openbsd \
#       nodejs \
#       openssl \
#       pkgconfig \
#       postgresql-dev \
#       python2 \
#       tzdata \
#       yarn \
#       zip \
#       imagemagick \
#       poppler-utils

RUN echo "precedence  2a04:4e42::0/32  5" >> /etc/gai.conf
RUN gem install bundler -v 2.0.2

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

RUN curl -L https://github.com/chrisvel/wreeto_official/archive/refs/tags/v${VERSION}.tar.gz | tar zx --strip-components 1 -C /app/code

RUN bundle config build.nokogiri --use-system-libraries
RUN bundle update --conservative mimemagic 
RUN bundle check || bundle install --jobs 20 --retry 5
RUN bundle exec rake assets:precompile

RUN rm -rf /app/code/tmp && ln -s /run/wreeto/tmp /app/code/tmp && \
    rm -rf /app/code/log && ln -s /run/wreeto/log /app/code/log

RUN sed -e 's/^port/#port/' -i /app/code/config/puma.rb
RUN echo 'bind "tcp://0.0.0.0:3000"' >> /app/code/config/puma.rb

COPY start.sh /app/pkg

CMD [ "/app/pkg/start.sh"]
